Gott nahe zu sein ist das Bedürfnis vieler Menschen. Das Lesen in Seinem Wort ist eine besonders geeignete Möglichkeit dafür. Seit einiger Zeit gibt es hierzu auch praktische Apps mit verschiedenen Bibelübersetzungen zum Lesen und studieren, mit Leseplänen, Gebeten und vieles mehr.

Ein alter Brauch ist das sogenannte „Bibelstechen“, bei dem die Bibel ganz einfach aufgeschlagen und an der solchermaßen getroffenen Bibelstelle ein Vers oder Kapitel gelesen wird.

Diese App wurde entwickelt, um für das Bibelstechen eine praktische Alternative anzubieten:
Wenn Sie darin das "Drück mich"-Feld antippen, erscheint ein Buch mit Kapitelangabe.
Sie können dieses ausgeloste Bibelkapitel mithilfe einer Bibel-App Ihrer Wahl, die Sie sich zusätzlich installieren oder in Ihrer Papierbibel lesen.

Die Bibel-Kapitel-Losung App ist kostenlos und werbungsfrei, sie erfordert keine Zugriffsrechte, arbeitet offline, verlangt keine Registrierung und ist beliebig oft nutzbar.

Programmtechnisch wird ermittelt, wie viele Millisekunden (tausendstel Sekunden) seit dem 1. Januar 1970 bis zum Antippen des "Drück mich"-Feldes vergangen sind und mit dieser Zahl wird ein sogenannter Pseudozufallsgenerator gefüttert, was schlussendlich zur Ausgabe eines Bibelkapitels führt.

Damit kann es als „zeitlich“ pseudozufälliges Pendant zum gebräuchlichen „räumlichen“ Bibelstechen zur Findung einer Bibelstelle aufgefasst werden.


Über eine Wertschätzung würde ich mich sehr freuen:

Paypal: 
paypal.me/BerndSpeth
Wird als Zahlungsart „Geld an einen Freund senden“ bzw. „Für Freunde und Familie“ ausgewählt, fallen keine Gebühren an. 
Bei „Wofür ist diese Zahlung?“ bitte angeben: Freiwillige Unterstützung BiKaLo

Bank:
Zahlungsempfänger: Bernd Speth
IBAN: DE38 6906 1800 0000 5040 09
BIC: GENODE61UBE
Bank: Volksbank Überlingen
Verwendungszweck: Freiwillige Unterstützung BiKaLo

Herzlichen Dank!

Bernd Speth
Manzweg 5
88662 Überlingen
speth.b@gmx.de
