Hint: This is a German app, but because your device locale is not German, you get its description in English here:

Being close to God is the desire of many people. Reading in His Word is a particularly suitable possibility for it. For some time, there have also been practical apps with various Bible translations for reading and studying, reading schedules, prayers and much more.

An old custom is the so-called “Bibelstechen” (Bible drawing), in which the Bible is simply opened and a verse or chapter is read at the Bible passage that has been struck in this way.

This app is designed to offer a practical alternative for this kind of Bible drawing:
If you touch onto the "Touch me" button, a book with chapter indication appears.
To read this drawn Bible chapter you can use a Bible app of your choice or read it in your paper Bible.

The "Bibel-Kapitel-Losung" (Bible chapter drawing) app is costless and advertising-free, it does not require access rights, works offline, does not require registration and can be used as often as desired.

German app, English version soon.
